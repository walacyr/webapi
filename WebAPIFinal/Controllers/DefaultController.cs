﻿using Cliente.Class;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Script.Serialization;


namespace WebApiFinal.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DefaultController : ApiController
    {
        // GET: api/Default
        public IHttpActionResult Get()
        {
            try
            {
                return Ok(new Client().getAll());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // GET: api/Default/5
        public IHttpActionResult Get(int id)
        {
            try
            {
                Client _return = new Client(id);
                if (_return.cod == 0)
                {
                    return NotFound();
                }
                return Ok(_return);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // POST: api/Default
        public IHttpActionResult Post([FromBody]Client obj)
        {
            try
            {
                Client c = new Client();
                c.Name = obj.Name;
                c.Type = obj.Type;
                c.Email = obj.Email;
                c.Insert();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT: api/Default/5
        public IHttpActionResult Put(int id, [FromBody]Client obj)
        {
            try
            {
                Client c = new Client(id);
                c.Name = obj.Name;
                c.Type = obj.Type;
                c.Email = obj.Email;
                c.Update();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // DELETE: api/Default/5
        public IHttpActionResult Delete(int id)
        {
            Client c = new Client(id);
            try
            {
                c.Delete();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
