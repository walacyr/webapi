﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;

namespace Cliente.Class
{
    public partial class Client : IDisposable
    {
        public Client()
        {
            this._isNew = true;
            this._isModified = false;
        }
        public Client(int cod)
        {
            using (SqlConnection connection = connect())
            {
                try
                {
                    connection.Open();
                }
                catch (Exception)
                {
                    throw;
                }
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "Select * From Client Where Cod = @Cod";
                    command.Parameters.AddWithValue("@Cod", cod);
                    using (SqlDataReader datareader = command.ExecuteReader())
                    {
                        if(datareader.HasRows)
                        {
                            datareader.Read();
                            this._cod = datareader.GetInt32(datareader.GetOrdinal("Cod"));
                            this._name = datareader.GetString(datareader.GetOrdinal("Name"));
                            this._type = datareader.GetInt32(datareader.GetOrdinal("Type"));
                            this._email = datareader.GetString(datareader.GetOrdinal("Email"));
                            this._cadDate = datareader.GetDateTime(datareader.GetOrdinal("CadDate"));
                        }
                    }
                }
            }
            this._isNew = false;
            this._isModified = false;
        }
        public void Insert()
        {
            using (SqlConnection connection = connect())
            {
                try
                {
                    connection.Open();
                }
                catch (Exception)
                {
                    throw;
                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Insert Into Client (Cod, Name, Type, Email, CadDate) Values (@Cod, @Name, @Type, @Email, @CadDate)";
                    cmd.Connection = connection;

                    cmd.Parameters.AddWithValue("@Cod", Next());
                    cmd.Parameters.AddWithValue("@Name", this._name);
                    cmd.Parameters.AddWithValue("@Type", this._type);
                    cmd.Parameters.AddWithValue("@Email", this._email);
                    cmd.Parameters.AddWithValue("@CadDate", DateTime.Now);

                    try
                    {
                        cmd.ExecuteNonQuery();
                        this._isNew = false;
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
        }
        public void Update()
        {
            using (SqlConnection connection = connect())
            {
                try
                {
                    connection.Open();
                }
                catch (Exception)
                {
                    throw;
                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Update Client Set Name = @Name, Type = @Type, Email = @Email Where Cod = @Cod";
                    cmd.Connection = connection;

                    cmd.Parameters.AddWithValue("@Name", this._name);
                    cmd.Parameters.AddWithValue("@Type", this._type);
                    cmd.Parameters.AddWithValue("@Email", this._email);
                    cmd.Parameters.AddWithValue("@Cod", this._cod);

                    try
                    {
                        cmd.ExecuteNonQuery();
                        this._isModified = false;
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
        }
        public void Delete()
        {
            using (SqlConnection connection = connect())
            {
                try
                {
                    connection.Open();
                }
                catch (Exception)
                {
                    throw;
                }
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Delete From Client Where Cod = @Cod";
                    cmd.Connection = connection;

                    cmd.Parameters.AddWithValue("@Cod", cod);
                    
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
        }
        public void gravar()
        {
            if(this._isNew)
            {
                Insert();
            }
            else if(_isModified)
            {
                Update();
            }
        }
        public static Int32 Next()
        {
            Int32 _return = 0;
            using (SqlConnection connection = connect())
            {
                try
                {
                    connection.Open();
                }
                catch (Exception)
                {
                    throw;
                }
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "Select COALESCE(MAX(cod), 0) From Client";
                    using (SqlDataReader datareader = command.ExecuteReader())
                    {
                        if (datareader.HasRows)
                        {
                            datareader.Read();
                            _return = datareader.GetInt32(0) + 1;
                        }
                    }
                }
            }
            return _return;
        }
        public List<Client> getAll()
        {
            List<Client> _return = new List<Client>();
            using (SqlConnection connection = connect())
            {
                try
                {
                    connection.Open();
                }
                catch (Exception)
                {
                    throw;
                }
                using (SqlCommand command = new SqlCommand())
                {
                    command.Connection = connection;
                    command.CommandText = "Select * From Client";

                    using (SqlDataReader datareader = command.ExecuteReader())
                    {
                        if (datareader.HasRows)
                        {
                            while(datareader.Read())
                            {
                                Client c = new Client();
                                c._cod = datareader.GetInt32(datareader.GetOrdinal("Cod"));
                                c._name = datareader.GetString(datareader.GetOrdinal("Name"));
                                c._email = datareader.GetString(datareader.GetOrdinal("Email"));
                                c._type = datareader.GetInt32(datareader.GetOrdinal("Type"));
                                c._cadDate = datareader.GetDateTime(datareader.GetOrdinal("CadDate"));
                                if (datareader == null)
                                {
                                    return new List<Client>();
                                }
                                _return.Add(c);
                            }
                        }
                    }
                }
            }
            return _return;
        }
        public static SqlConnection connect()
        {
            SqlConnection connection = new SqlConnection("Data Source=den1.mssql6.gear.host;" + "Initial Catalog=clientwebdb;" + "User id=clientwebdb;" + "Password=Dy9JpJg-!9tY;");
            return connection;
        }
        public void Dispose()
        {
            this.gravar();
        }
    }
}
