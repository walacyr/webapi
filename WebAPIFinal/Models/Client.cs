﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Cliente.Class
{
    public partial class Client
    {
        private bool _isNew;
        [Browsable(false)]
        public bool isNew
        {
            get { return _isNew; }
        }

        private bool _isModified;
        [Browsable(false)]
        public bool isModified
        {
            get { return _isModified; }
        }

        private int _cod;
        [DataObjectField(true, true, false)]
        public int cod
        {
            get { return _cod; }
            set
            {
                _cod = value;
                this._isModified = true;
            }
        }

        private string _name;
        [DataObjectField(false, false, true)]
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                this._isModified = true;
            }
        }

        private int _type;
        [DataObjectField(false, false, true)]
        public int Type
        {
            get { return _type; }
            set
            {
                _type = value;
                this._isModified = true;
            }
        }

        private string _email;
        [DataObjectField(false, false, true)]
        public string Email
        {
            get { return _email; }
            set
            {
                _email = value;
                this._isModified = true;
            }
        }

        private DateTime _cadDate;
        [DataObjectField(false, false, true)]
        public DateTime CadDate
        {
            get { return _cadDate; }
            set
            {
                _cadDate = value;
                this._isModified = true;
            }
        }
    }
}
